# -*- coding: utf-8 -*-
"""
Created on Thu Aug 29 12:30:52 2019

@author: Mayuri
"""

def str2num(input_value):
    if(input_value.isnumeric()):
        print("the entered input is integer")
        return int(input_value)
    try:
        float_value = float(input_value)
        print("the entered input is a float")
        return float_value
    except:
        pass
    
    print("entered input is neither integer nor float")
    raise ValueError