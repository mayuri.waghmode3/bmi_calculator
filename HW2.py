#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 29 18:44:30 2019


@author: Mayuri Waghmode
@sauid: 999900522
@date: 8/30/2019

"""


from mymodule import str2num
try:
    print("Please enter your weight:")
    weight = str2num(input())
    print("Please enter your height:")
    height = str2num(input())
    bmi = weight*703/height/height
    print("Your weight = " + str(weight))
    print("Your height = " + str(height))
    print("Your BMI = " + str(bmi))
except:
    print("An exception has occurred, use %tb to see the full traceback.SystemExit: 1")

