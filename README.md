# BMI_calculator in python
*  created a python source file mymodule.py
*  created a function named str2num in mymodule.py 
* Your str2num should do the following three situations
>        a. if the input is an integer 
>               convert the string to integer and return the integer
>               print out the entered input is integer
>        b. if the input is a float
>               convert the string to float and return the float
>               print out the entered input is a float
>        c. if the input is neither an integer nor a float
>               print out the entered input is neither integer nor float
>               exit the program
